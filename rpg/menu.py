from collections import OrderedDict
from django.conf import settings
from rules.headers import HEADERS as RULES_HEADERS

HEADERS = OrderedDict()

# Eventually, I'll likely change this to have the apps as the keys, and the items as the values, but for now, we have enough room to have more options
for key, value in RULES_HEADERS.items():
    HEADERS[key] = value

HEADERS["Tools"] = {
	"Name Generator": "generators/"
}

HEADERS['License'] = {
    "OGL": "license/ogl.html",
    "AGPL": "license/agpl.html",
}

SRC_URL = getattr(settings, "SRC_URL", None)
if SRC_URL:
    HEADERS['License']['Source'] = SRC_URL

def get_standard_headers():
    return HEADERS

def get_standard_context(page_model):
    return {
        'base': '/',
        'page_model': page_model
    }

