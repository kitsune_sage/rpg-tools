name: Wizard
sources:
  - pathfinder-roleplaying-game-core-rulebook
tags:
  - Arcane
max_level: 20
hit_die: 6
starting_wealth: 2d6 × 10 gp (average 70 gp)
class_skills:
  - Appraise
  - Craft
  - Fly
  - Knowledge (dungeoneering)
  - Knowledge (engineering)
  - Knowledge (geography)
  - Knowledge (history)
  - Knowledge (local)
  - Knowledge (nature)
  - Knowledge (nobility)
  - Knowledge (planes)
  - Knowledge (religion)
  - Linguistics
  - Profession
  - Spellcraft
skill_ranks_per_level: 2
bab_progression: slow
fort_progression: poor
ref_progression: poor
will_progression: good
spells_per_day: wizard
spell_list: wizard
spell_list_levels: 9
weapon_proficiencies:
  - club
  - dagger
  - heavy crossbow
  - light crossbow
  - quarterstaff
class_features:
  - name: Spells
    levels:
      - 1
    text: |
      A wizard casts arcane spells drawn from the sorcerer/wizard spell list
      presented in Chapter 10. A wizard must choose and prepare his spells
      ahead of time.

      To learn, prepare, or cast a spell, the wizard must have an Intelligence
      score equal to at least 10 + the spell level. The Difficulty Class for a
      saving throw against a wizard’s spell is 10 + the spell level + the
      wizard’s Intelligence modifier.

      A wizard can cast only a certain number of spells of each spell level per
      day. His base daily spell allotment is given on Table 3–16. In addition,
      he receives bonus spells per day if he has a high Intelligence score (see
      Table 1–3).

      A wizard may know any number of spells. He must choose and prepare his
      spells ahead of time by getting 8 hours of sleep andspending 1 hour
      studying his spellbook. While studying, the wizard decides which spells
      to prepare.
  - name: Bonus Languages
    levels:
      - 1
    text: |
      A wizard may substitute Draconic for one of the bonus languages available
      to the character because of his race.
  - name: Arcane Bond
    levels:
      - 1
    text: |
      At 1st level, wizards form a powerful bond with an object or a creature.
      This bond can take one of two forms: a familiar or a bonded object. A
      familiar is a magical pet that enhances the wizard’s skills and senses
      and can aid him in magic, while a bonded object is an item a wizard can
      use to cast additional spells or to serve as a magical item. Once a
      wizard makes this choice, it is permanent and cannot be changed. Rules
      for familiars appear on page 82, while rules for bonded items are given
      below.

      Wizards who select a bonded object begin play with one at no cost.
      Objects that are the subject of an arcane bond must fall into one of the
      following categories: amulet, ring, staff, wand, or weapon. These objects
      are always masterwork quality. Weapons acquired at 1st level are not made
      of any special material. If the object is an amulet or ring, it must be
      worn to have effect, while staves, wands, and weapons must be held in one
      hand. If a wizard attempts to cast a spell without his bonded object worn
      or in hand, he must make a concentration check or lose the spell. The DC
      for this check is equal to 20 + the spell’s level. If the object is a
      ring or amulet, it occupies the ring or neck slot accordingly.

      A bonded object can be used once per day to cast any one spell that the
      wizard has in his spellbook and is capable of casting, even if the spell
      is not prepared. This spell is treated like any other spell cast by the
      wizard, including casting time, duration, and other effects dependent on
      the wizard’s level. This spell cannot be modified by metamagic feats or
      other abilities. The bonded object cannot be used to cast spells from the
      wizard’s opposition schools (see arcane school).

      A wizard can add additional magic abilities to his bonded object as if he
      has the required item creation feats and if he meets the level
      prerequisites of the feat. For example, a wizard with a bonded dagger
      must be at least 5th level to add magic abilities to the dagger (see the
      Craft Magic Arms and Armor feat in Chapter 5). If the bonded object is a
      wand, it loses its wand abilities when its last charge is consumed, but
      it is not destroyed and it retains all of its bonded object properties
      and can be used to craft a new wand. The magic properties of a bonded
      object, including any magic abilities added to the object, only function
      for the wizard who owns it. If a bonded object’s owner dies, or the item
      is replaced, the object reverts to being an ordinary masterwork item of
      the appropriate type.

      If a bonded object is damaged, it is restored to full hit points the next
      time the wizard prepares his spells. If the object of an arcane bond is
      lost or destroyed, it can be replaced after 1 week in a special ritual
      that costs 200 gp per wizard level plus the cost of the masterwork item.
      This ritual takes 8 hours to complete. Items replaced in this way do not
      possess any of the additional enchantments of the previous bonded item. A
      wizard can designate an existing magic item as his bonded item. This
      functions in the same way as replacing a lost or destroyed item except
      that the new magic item retains its abilities while gaining the benefits
      and drawbacks of becoming a bonded item.
  - name: Arcane School
    type: Ex
    levels:
      - 1
    text: |
      A wizard can choose to specialize in one school of magic, gaining
      additional spells and powers basedon that school. This choice must be
      made at 1st level, and once made, it cannot be changed. A wizard that
      does not select a school receives the universalist school instead.

      A wizard that chooses to specialize in one school of magic must select
      two other schools as his opposition schools, representing knowledge
      sacrificed in one area of arcane lore to gain mastery in another. A
      wizard who prepares spells from his opposition schools must use two spell
      slots of that level to prepare the spell. For example, a wizard with
      evocation as an opposition school must expend two of his available
      3rd-level spell slots to prepare a fireball. In addition, a specialist
      takes a –4 penalty on any skill checks made when crafting a magic item
      that has a spell from one of his opposition schools as a prerequisite. A
      universalist wizard can prepare spells from any school without
      restriction.

      Each arcane school gives the wizard a number of school powers. In
      addition, specialist wizards receive an additional spell slot of each
      spell level he can cast, from 1st on up. Each day, a wizard can prepare a
      spell from his specialty school in that slot. This spell must be in the
      wizard’s spellbook. A wizard can select a spell modified by a metamagic
      feat to prepare in his school slot, but it uses up a higher-level spell
      slot. Wizards with the universalist school do not receive a school slot.
    options: wizard-schools
  - name: Cantrips
    type: Su
    levels:
      - 1
    text: |
      Wizards can prepare a number of cantrips, or 0-level spells, each day, as
      noted on Table 3–16 under “Spells per Day.” These spells are cast like
      any other spell, but they are not expended when cast and may be used
      again. A wizard can prepare a cantrip from an opposed school, but it uses
      up two of his available slots (see below).
  - name: Scribe Scroll
    levels:
      - 1
    text: At 1st level, a wizard gains Scribe Scroll as a bonus feat.
  - name: Spellbooks
    levels:
      - 1
    text: |
      A wizard must study his spellbook each day to prepare his spells. He
      cannot prepare any spell not recorded in his spellbook, except for read
      magic, which all wizards can prepare from memory.

      A wizard begins play with a spellbook containing all 0-level wizard
      spells (except those from his opposed schools, if any; see Arcane
      Schools) plus three 1st-level spells of his choice. The wizard also
      selects a number of additional 1st-level spells equal to his Intelligence
      modifier to add to the spellbook. At each new wizard level, he gains two
      new spells of any spell level or levels that he can cast (based on his
      new wizard level) for his spellbook. At any time, a wizard can also add
      spells found in other wizards’ spellbooks to his own (see Chapter 9).
  - name: Bonus Feats
    type: Su
    levels:
      - 5
      - 10
      - 15
      - 20
    text: |
      At 5th, 10th, 15th, and 20th level, a wizard gains a bonus feat. At each
      such opportunity, he can choose a metamagic feat, an item creation feat,
      or Spell Mastery. The wizard must still meet all prerequisites for a
      bonus feat, including caster level minimums. These bonus feats are in
      addition to the feats that a character of any class gets from advancing
      levels. The wizard is not limited to the categories of item creation
      feats, metamagic feats, or Spell Mastery when choosing those feats.
text: |
  Beyond the veil of the mundane hide the secrets of absolute power. The works
  of beings beyond mortals, the legends of realms where gods and spirits tread,
  the lore of creations both wondrous and terrible—such mysteries call to those
  with the ambition and the intellect to rise above the common folk to grasp
  true might. Such is the path of the wizard. These shrewd magic-users seek,
  collect, and esoteric knowledge, drawing on cultic arts to work wonders
  beyond the abilities of mere mortals. While some might choose a particular
  field of magical study and become masters of such powers, others embrace
  versatility, reveling in the unbounded wonders of all magic. In either case,
  wizards prove a cunning and potent lot, capable of smiting their foes,
  empowering their allies, and shaping the world to their every desire.

  **Role:** While universalist wizards might study to prepare themselves for
  any manner of danger, specialist wizards research schools of magic that make
  them exceptionally skilled within a specific focus. Yet no matter their
  specialty, all wizards are masters of the impossible and can aid their allies
  in overcoming any danger.

  **Alignment:** Any
