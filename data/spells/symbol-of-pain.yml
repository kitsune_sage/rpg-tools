name: Symbol of Pain
sources:
  - pathfinder-roleplaying-game-core-rulebook
  - pathfinder-roleplaying-game-ultimate-magic
functions_as: symbol-of-death
school: necromancy
descriptors:
  - evil
  - pain
spell_lists:
  - name: occultist
    level: 4
  - name: cleric
    level: 5
  - name: sorcerer/wizard
    level: 5
casting_time: 10 minutes
components:
  - V
  - S
  - |
      M (mercury and phosphorus, plus powdered diamond and opal worth a total
      of 1,000 gp)
text: |
  This spell functions like [*symbol of death*](/spells/symbol-of-death/),
  except that each creature within the radius of a *symbol of pain* instead
  suffers wracking pains that impose a --4 penalty on attack rolls, skill
  checks, and ability checks. These effects last for 1 hour after the creature
  moves farther than 60 feet from the symbol.

  Unlike *symbol of death*, *symbol of pain* has no hit point limit; once
  triggered, a symbol of pain simply remains active for 10 minutes per caster
  level.

  *Note:* Magic traps such as *symbol of pain* are hard to detect and disable.
  While any character can use [Perception](/skills/perception.html) to find a
  symbol, only a character with the trapfinding class feature can use [Disable
  Device](/skills/disable-device.html) to disarm it. The DC in each case is 25
  + spell level, or 30 for *symbol of pain*.
