name: Spellcraft
sources:
  - pathfinder-roleplaying-game-core-rulebook
ability_score: Int
trained_only: true
brief_reference: |
  | Task                                                                               | Spellcraft DC            |
  |:-----------------------------------------------------------------------------------|:------------------------:|
  | Identify a spell as it is being cast                                               | 15 + Spell Level         |
  | Learn a spell from a spellbook or scroll                                           | 15 + Spell Level         |
  | Prepare a spell from a borrowed spellbook                                          | 15 + Spell Level         |
  | Identify the properties of a magic item using [Detect Magic](/spells/detect-magic) | 15 + item's caster level |


text: |
  You are skilled at the art of casting spells, identifying magic items,
  crafting magic items, and identifying spells as they are being cast.

  | Task                                                                               | Spellcraft DC            |
  |:-----------------------------------------------------------------------------------|:------------------------:|
  | Identify a spell as it is being cast                                               | 15 + Spell Level         |
  | Learn a spell from a spellbook or scroll                                           | 15 + Spell Level         |
  | Prepare a spell from a borrowed spellbook                                          | 15 + Spell Level         |
  | Identify the properties of a magic item using [Detect Magic](/spells/detect-magic) | 15 + item's caster level |

  Spellcraft is used whenever your knowledge and skill of the technical art of
  casting a spell or crafting a magic item comes into question. This skill is
  also used to identify the properties of magic items in your possession
  through the use of spells such as detect magic and identify. The DC of this
  check varies depending upon the task at hand.

  **Craft Magic Items**

  Making a Spellcraft check to craft a magic item is made as part of the
  creation process.

  **Determine Properties of Magic Item**

  Attempting to ascertain the properties of a magic item takes 3 rounds per
  item to be identified and you must be able to thoroughly examine the object.

  Retry? When using detect magic or identify to learn the properties of magic
  items, you can only attempt to ascertain the properties of an individual item
  once per day. Additional attempts reveal the same results.

  **Identify Spell Being Cast**

  Identifying a spell as it is being cast requires no action, but you must be
  able to clearly see the spell as it is being cast, and this incurs the same
  penalties as a Perception skill check due to distance, poor conditions, and
  other factors.

  Retry? You cannot retry checks made to identify a spell.

  **Learn Spell from Spellbook**

  Learning a spell from a spellbook takes 1 hour per level of the spell
  (0-level spells take 30 minutes).

  Retry? If you fail to learn a spell from a spellbook or scroll, you must wait
  at least 1 week before you can try again.

  **Prepare Spell from Borrowed Spellbook**

  Preparing a spell from a borrowed spellbook does not add any time to your
  spell preparation.

  Retry? If you fail to prepare a spell from a borrowed spellbook, you cannot
  try again until the next day.

  **Modifiers**

  - Class If you are a specialist wizard, you get a +2 bonus on Spellcraft checks made to identify, learn, and prepare spells from your chosen school. Similarly, you take a –5 penalty on similar checks made concerning spells from your opposition schools.
  - Race An elf gets a +2 racial bonus on Spellcraft checks to identify the properties of magic items.
  - Feats If you have the Magical Aptitude feat, you get a +2 bonus on all Spellcraft checks. If you have 10 or more ranks in Spellcraft, the bonus increases to +4.
