name: Perception
sources:
  - pathfinder-roleplaying-game-core-rulebook
ability_score: Wis
text: |
  Your Perception allows you to notice fine details and alert you to danger.
  Perception covers all five senses, including sight, hearing, touch, taste,
  and smell.

  Most Perception checks are reactive, made in response to observable stimulus.
  Intentionally searching for something is a move action.

  Creatures with the scent special quality have a +8 bonus on Perception checks
  made to detect a scent. Creatures with the tremorsense special quality have a
  +8 bonus on Perception checks against creatures touching the ground and
  automatically make any such checks within their range.

  **Notice Details:** Perception is used to notice fine details in the
  environment. The DC to notice such details varies depending upon distance,
  the environment, and how noticeable the detail is. The following tables give
  a number of guidelines.

  | Detail                                         |               DC               |
  |:-----------------------------------------------|:------------------------------:|
  | Hear the sound of battle                       |              --10              |
  | Notice the stench of rotting garbage           |              --10              |
  | Detect the smell of smoke                      |               0                |
  | Hear the details of a conversation             |               0                |
  | Notice a visible creature                      |               0                |
  | Determine if food is spoiled                   |               5                |
  | Hear the sound of a creature walking           |               10               |
  | Hear the details of a whispered conversation   |               15               |
  | Find the average concealed door                |               15               |
  | Hear the sound of a key being turned in a lock |               20               |
  | Find the average secret door                   |               20               |
  | Hear a bow being drawn                         |               25               |
  | Sense a burrowing creature underneath you      |               25               |
  | Notice a pickpocket                            |   Opposed by Sleight of Hand   |
  | Notice a creature using Stealth                |       Opposed by Stealth       |
  | Find a hidden trap                             |         Varies by trap         |
  | Identify the powers of a potion through taste  | 15 + the potion's caster level |

  | Perception Modifiers                        |        DC Modifier        |
  |:--------------------------------------------|:-------------------------:|
  | Distance to the source, object, or creature |      +1 per 10 feet       |
  | Through a closed door                       |            +5             |
  | Through a wall                              | +10 per foot of thickness |
  | Favorable conditions                        |            --2            |
  | Unfavorable conditions                      |            +2             |
  | Terrible conditions                         |            +5             |
  | Creature making the check is distracted     |            +5             |
  | Creature making the check is asleep         |            +10            |
  | Creature or object is invisible             |            +20            |

  Favorable and unfavorable conditions depend upon the sense being used to make
  the check. For example, bright light might decrease the DC of checks
  involving sight, while torchlight or moonlight might increase the DC.
  Background noise might increase a DC involving hearing, while competing odors
  might increase the DC of a check involving scent.

  Terrible conditions are as for unfavorable conditions, but more extreme. For
  example, candlelight for DCs involving sight, a roaring dragon for DCs
  involving hearing, and an overpowering stench covering the area for DCs
  involving scent.

  EDITORS NOTE: There is additional information in [Ultimate Intrigue](sources/pathfinder-roleplaying-game-ultimate-intrigue)
  #TODO Add that information
