"""CK Character Sheet

These are tools to generate a character sheet.
"""

import pycli

from rules.data_models import *
from rules.data_readers import read_dataclass, read_dir
from rules.parsers.d20 import parse_site
from rules.pretty_serializer import serialize_dataclass

data_types = {
    "archetype": Archetype,
    "armor": Armor,
    "class": BaseClass,
    "feat": Feat,
    "item": Item,
    "race": Race,
    "skill": Skill,
    "source": Source,
    "spell": Spell,
    "weapon": Weapon,
    "weaponability": WeaponAbility,
    "domain": "domains",
    "school": "wizard-schools",
    "blessing": "warpriest-blessings",
    "bloodlines": "bloodlines",
    "patron": "patrons",
    "talents": "rogue-talents",
    "advancedtalents": "rogue-advanced-talents",
    "ragepowers": "rage-powers",
    'slayer': 'slayer-talents',
}

def cleanup(**kwargs):
    """Cleanup the requested data directory by pretty print the requested files by in the directory

    Args:
        archetype (str, optional): The archetypes to clean. Defaults to None.
        armor (str, optional): . Defaults to None.
        class (str, optional): The base class to clean. Defaults to None.
        feat (str, optional): The feat to clean. Defaults to None.
        item (str, optional): The item to clean. Defaults to None.
        race (str, optional): The race to clean. Defaults to None.
        skill (str, optional): The skill to clean. Defaults to None.
        source (str, optional): The source to clean. Defaults to None.
        spell (str, optional): The spell to clean. Defaults to None.
        weapon (str, optional): The weapon to clean. Defaults to None.
        domain (str, optional): The domain to clean. Defaults to None.
        school (str, optional): The wizard school to clean. Defaults to None.
        patron (str, optional): The patron to clean. Defaults to None.
        blessing (str, optional): The blessings to clean. Defaults to None.
        bloodlines (str, optional): The bloodlines to clean. Defaults to None.
        talents (str, optional): The rogue talents to clean. Defaults to None.
        advancedtalents (str, optional): The advanced talents to clean. Defaults to None.
        ragepowers (str, optional): The rage Powers to clean. Defaults to None.
        weaponability (str, optional): The Weapon Ability to clean. Defaults to None.
    """
    for data_type_key, current_type in data_types.items():
        if not data_type_key in kwargs:
            continue

        arg_value = kwargs[data_type_key]
        if not arg_value:
            continue

        use_single_lines = data_type_key == "source"

        data_dir = None
        if isinstance(current_type, str):
            data_dir = os.path.join("data", current_type)
            current_type = ClassChoices
        else:
            data_dir = current_type.DIRECTORY

        if arg_value == "*":
            for data in read_dir(current_type, data_dir):
                serialize_dataclass(data, data_dir, single_line_strings=use_single_lines)
        else:
            for id_to_clean in arg_value.split(','):
                data = read_dataclass(current_type, id_to_clean, data_dir)
                serialize_dataclass(data, data_dir, single_line_strings=use_single_lines)


def parse(url: str, new_id: str=None, source: str=None):
    """Parse the webpage at the given url

    Args:
        url (str): The url to parse
        new_id (str, optional): The id to save the parsed object as. Defaults to None.
        source (str, optional): The source book for the parsed object. Defaults to None.
    """
    data = parse_site(url, new_id, source)
    serialize_dataclass(data)


def add_subparsers(parent, handler, arguments):
    sub_parser = parent.add_parser(handler.__name__, help=handler.__doc__)
    for argument in arguments:
        sub_parser.add_argument(argument[0], **argument[1])
    sub_parser.set_defaults(func=handler)


if __name__ == "__main__":
    cli = pycli.Cli(time=True)
    sub_parsers = cli.init_subparser(parse, cleanup)

    cli.start_cli()
