FROM python:3.12-alpine

# Build time
ARG BUILD_SRC_URL
# Run time
ENV SRC_URL=$BUILD_SRC_URL

COPY ./requirements* /app/
WORKDIR /app

RUN pip install -r requirements_prod.txt
COPY . .
RUN python manage.py collectstatic

EXPOSE 8000

CMD ["gunicorn", "-b", "0.0.0.0:8000", "rpg.wsgi"]

