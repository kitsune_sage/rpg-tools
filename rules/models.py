from django.db import models
from django.utils.translation import gettext_lazy

class CommonModel(models.Model):
    # TODO: can/should be SlugField
    fragment = models.CharField(max_length=50, unique=True, db_index=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.fragment

class AbilityScores(models.TextChoices):
    STRENGTH = 'STR', gettext_lazy('Strength')
    DEXTERITY = 'DEX', gettext_lazy('Dexterity')
    CONSTITUTION = 'CON', gettext_lazy('Constitution')
    WISDOM = 'WIS', gettext_lazy('Wisdom')
    INTELLIGENCE = 'INT', gettext_lazy('Intelligence')
    CHARISMA = 'CHA', gettext_lazy('Charisma')

class Source(CommonModel):
    name = models.CharField(max_length=250)
    section_15 = models.TextField()
    copyright_year = models.SmallIntegerField()
    publisher = models.CharField(max_length=100)
    authors = models.TextField()

class Skill(CommonModel):
    name = models.CharField(max_length=50)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, to_field='fragment', related_name='skills')
    ability_score = models.CharField(max_length=3, choices=AbilityScores.choices)
    armor_check_penalty = models.BooleanField()
    trained_only = models.BooleanField()
    text = models.TextField()
    # TODO: impletement tags, although, I'm not sure what tags are useful for skills.
    # tags = models.TextField()


""" On hold till Skills are finished.
class BaseClass(CommonModel):
    name = models.CharField(max_length=50)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, to_field='fragment', related_name='classes')
    # TODO: Impletement tags
    # tags = models.ForeignKey(Tags)
    max_level = models.SmallIntegerField()
    hit_die = models.SmallIntegerField()
    prerequisites = models.TextField()


class ClassFeature(CommonModel):
    parent_class = models.ForeignKey(BaseClass, on_delete=models.CASCADE, to_field='fragment', related_name='features')
"""


"""
class Tags(models.Model):
    name = models.CharField()
"""

""" TODO: This isn't needed for first pass, but may want to add
class AgeInfo():
    adulthood = models.IntegerField()
    intuitive = models.CharField()
    self_taught = models.CharField()
    trained = models.CharField()
    middle_age = models.IntegerField()
    old = models.IntegerField()
    venerable = models.IntegerField()
    maximum = models.CharField()

class GenderBase():
    name = models.CharField()
    base_height = models.CharField()
    base_weight = models.CharField()
    height_modifier = models.CharField()

@dataclass
class Measurements():
    weight_modifier = models.CharField()
    genders: List[GenderBase]
"""

"""
class ClassChoice():
    name: models.CharField()
    sources: models.ManyToManyField(Source)
    choice_type: models.CharField()
    text: models.TextField()
    features: models.TextField()

class Prerequisite():
    feat:
"""

# class Feature(CommonModel):
#     """The name of the feature."""
#     name: models.CharField()
#     """The type of the Feature (Ex, Sp, or Su)"""
#     type: models.CharField()
#     """The levels that the feature adds abilities at."""
#     levels_text: models.TextField()
#     """The main text of the feature"""
#     text: models.TextField()
#     """The main benefit of the feature"""
#     benefit: models.TextField()
#     """The flavor text of the feature"""
#     flavor_text: models.TextField()
#     """Any Special information for the feature.
#
#     This is usually an additional bonus if you have a specific class."""
#     special: models.TextField()
#     """Relevant FAQ's for the feature"""
#     faq: models.TextField()
#     """"""
#     hidden: models.BooleanField()
#     options: models.CharField()
#
#
# # Create your models here.
# class Race(CommonModel):
#     sources: models.ManyToManyField(Source)
#     tags: models.ManyToManyField(Tags)
#
#     summary: str = models.TextField()
#     ability_scores: str = models.TextField()
#     type: str = models.CharField()
#     subtypes = models.CharField()
#     size: str = models.CharField()
#     # ages: AgeInfo = None
#     # height_and_weight: Measurements = None
#     text: str = models.TextField()
#     racial_traits: List[Feature] = field(default_factory=list)
#     alternate_traits: Dict[str, str] = field(default_factory=dict)
#     racial_subtypes: Dict[str, str] = field(default_factory=dict)
#     favored_class_bonus: Dict[str, str] = field(default_factory=dict)
