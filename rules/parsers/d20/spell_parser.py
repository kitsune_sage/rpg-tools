import logging
import re
from typing import Any, Dict, List

from rules.data_models import Spell, SpellListEntry

from bs4 import BeautifulSoup, Tag

COST_REGEX = re.compile(r"Cost *([\d,]*) ?gp" )

CASTING_MAP = {
	"Casting Time": "casting_time",
	"Components": "components",
}

EFFECT_MAP = {
	"Area": "area",
	"Range": "range",
	"Target": "target",
	"Duration": "duration",
	"Spell Resistance": "spell_resistance",
	"Saving Throw": "saving_throw",
}

logger = logging.getLogger(__name__)

def parse_spell(soup: BeautifulSoup, identifier, sources) -> Spell | None:
	if soup.article:
		article: Tag = soup.article
	else: 
		logger.warn("Failed to parse spell. No article found")
		return 

	if not article.h1:
		logger.warn("Failed to parse spell. No header found")
		return 

	name = article.h1.text
	lines = article.find_all('p')
	
	info = parse_school_and_lists(lines[0])
	
	lines = lines[1:]

	sections: Dict[str, List[Tag]] = dict()

	current_section: str = ""
	for line in lines:
		if 'class' in line.attrs and line.attrs['class'][0] == "divider":
			current_section = line.text
		else:
			if current_section in sections:
				sections[current_section].append(line)
			else:
				sections[current_section] = [line]

	text = ""

	if "DESCRIPTION" in sections:
		text = sections["DESCRIPTION"][0].text
		for line in sections["DESCRIPTION"][1:]:
			text += "\n\n" + line.text

	casting_info = dict()
	effect_info = dict()

	if "CASTING" in sections:
		if len(sections["CASTING"]) == 1:
			lines = split_on_tokens(sections["CASTING"][0].text, list(CASTING_MAP.keys()))
			casting_info = parse_sections_lines(lines, CASTING_MAP)
		else:
			casting_info = parse_sections_lines([x.text for x in sections["CASTING"]], CASTING_MAP)
	
	if "EFFECT" in sections:
		if len(sections["EFFECT"]) == 1:
			lines = split_on_tokens(sections["EFFECT"][0].text, list(EFFECT_MAP.keys()))
			effect_info = parse_sections_lines(lines, EFFECT_MAP)
		else:
			effect_info = parse_sections_lines([x.text for x in sections["EFFECT"]], EFFECT_MAP)

	info["id"] = identifier
	info["name"] = name
	info["text"] = text
	info["sources"] = sources
	

	info = info | casting_info | effect_info

	logger.warn(info)
	info["components"] = [ comp.strip() for comp in info["components"].split(',') ]

	spell = Spell(**info)

	logger.warn(spell)

	return spell


SPELL_SCHOOL_REGEX = re.compile(
	r"School (?P<school>\w*)"
	r"(?: \((?P<subschool>\w*)\))?"
	r"(?: \[(?P<descriptors>[\w-]*)\])?"
	r"; Level (?P<levels>(?:[\w/]* \d(?:, )?)*)"
)

"""Test strings:
School divination; Level bard 0, cleric 0, magus 0, sorcerer/wizard 0, witch 0
School enchantment (compulsion) [mind-affecting]; Level antipaladin 2, bard 2, bloodrager 3, cleric/oracle 2, inquisitor 2, shaman 2, sorcerer/wizard 3, medium 2, mesmerist 2, occultist 3, psychic 2, witch 2; Mystery godclaw 3
"""
def parse_school_and_lists(info_line: Tag) -> Dict[str, Any]:
	match = SPELL_SCHOOL_REGEX.match(info_line.text)

	if not match:
		logger.warning(f"The school description line was not in the expected format. {info_line.text}")
		return dict()

	info = dict()
	info["school"] = match.group("school")
	info["subschool"] = match.group("subschool")
	if match.group("descriptors"):
		info["descriptors"] = [match.group("descriptors")]
	info["spell_lists"] = list()

	spell_levels = match.group("levels")

	for class_level in spell_levels.split(","):
		class_level = class_level.strip()

		parts = class_level.split(' ')
		info["spell_lists"].append(SpellListEntry(
			name=parts[0], 
			level=int(parts[1])))

	return info

def split_on_tokens(line: str, tokens: List[str]) -> List[str]:
	indexes: List[int] = []
	for token in tokens:
		index = line.find(token)
		if token != -1:
			indexes.append(index)
	indexes.sort()

	if len(indexes) == 0:
		return []
	elif len(indexes) == 1:
		return [line]

	lines = []
	last = indexes[0]
	for i in indexes[1:]:
		lines.append(line[last:i].strip().strip(";"))
		last = i

	lines.append(line[last:].strip().strip(";"))

	return lines

def parse_casting_section(line: str) -> Dict[str, str]:
	lines = split_on_tokens(line, list(CASTING_MAP.keys()))
	return parse_sections_lines(lines, CASTING_MAP)

def parse_sections_lines(lines: List[str], map: Dict[str, str]) -> Dict[str, str]:
	info = dict()
	for line in lines:
		for k, v in map.items():
			if line.startswith(k):
				info[v] = line[len(k):].strip()

	return info

