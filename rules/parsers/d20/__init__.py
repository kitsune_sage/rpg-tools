from .item_parser import parse_item
from .class_parser import parse_class
from .spell_parser import parse_spell
from ...commands.cachesoup import CachedSoup

def parse_site(url, identifier, source):
    soup = CachedSoup(url)
    if "magic-items" in url:
        return parse_item(soup, identifier, source)
    if "classes" in url:
        return parse_class(soup, identifier, source)
    if "spell" in url:
        return parse_spell(soup, identifier, [source])
    else:
        raise ValueError("Unrecognized Url")
