from .data_models import Archetype, ArchetypeWrapper, BaseClass, Feat, Race, Skill, Spell, Weapon, WeaponAbility
from .data_models.pf import Character
from .data_readers import read_dataclass, read_dir, read_data
from rpg.menu import get_standard_context
from dataclasses import asdict
import logging

from .models import Source

from django.conf import settings

from django.http.request import HttpRequest

from django.http import (
	HttpResponse, 
	Http404
)
from django.shortcuts import render

# Create your views here.
logger = logging.getLogger(__name__)

def normalize_name(name):
	name = name.lower()
	name = name.replace(' ', '-')
	name = name.replace("'", '')
	name = name.replace('"', '')

	if name.endswith(".html"):
		name = name[:-5]
	elif name.endswith(".md"):
		name = name[:-3]
	elif name.endswith(".text"):
		name = name[:-5]
	elif name.endswith(".txt"):
		name = name[:-4]
	return name

def classes(request, class_name):
	class_name = normalize_name(class_name)
	base_class = read_dataclass(BaseClass, class_name, 'data/base-classes')
	context = get_standard_context(base_class)
	context['max_spell_levels'] = base_class.get_spells_per_day_at_level(base_class.max_level)
	return render(request, "rules/classes/_class.html.j2", context)

def archetypes(request, class_name, archetypes: str):

	class_name = normalize_name(class_name)
	class_info = read_dataclass(BaseClass, class_name, 'data/base-classes')

	for archetype in archetypes.split('/'):
		archetype = normalize_name(archetype)
		logger.warn(archetype)
		archetype = read_dataclass(Archetype, f"{class_name}_{archetype}")
		class_info = ArchetypeWrapper(class_info, archetype)

	context = get_standard_context(class_info)
	context['max_spell_levels'] = class_info.get_spells_per_day_at_level(class_info.max_level)
	return render(request, "rules/classes/_class.html.j2", context)

def feats(request, feat_name):
	feat_name = normalize_name(feat_name)
	feat = read_dataclass(Feat, feat_name, 'data/feats')
	context = get_standard_context(feat)
	return render(request, "rules/feats/_feat.html.j2", context)

def races(request, race_name):
	race_name = normalize_name(race_name)
	race = read_dataclass(Race, race_name, 'data/races')
	context = get_standard_context(race)

	return render(request, "rules/races/_race.html.j2", context)

def character(request, character_id):
	ext = get_ext(request)
	character_id = normalize_name(character_id)
	data = read_data("characters", character_id)

	data['id'] = character_id

	character = Character(character_id, data)

	context = get_standard_context(character)
	context['skills'] = read_dir(Skill)

	content_type = get_content_type(request)
	if ext == "text" or ext == "txt":

		context['summary'] = bool(request.GET.get('summary', False))
		return render(request, "rules/character/_layout.md.j2", context, content_type = "text/plain")

	return render(request, "rules/character/_layout.html.j2", context)

def get_spell_csv(spells):
	content = "Spell\tLevel\tRange\tSaving Throw\tDC\tSR\tCasting Time\tSchool\tArea/Target"

	# TODO: This is a quick fix to make Qucken Metamagic work for my one case. We need a better solution for metamagic
	highest_level = -1

	for spell in spells:
		level = -1
		name = spell.name
		casting_time = spell.casting_time
		school = spell.school
		for spell_list in spell.spell_lists:
			if spell_list.name == 'sorcerer/wizard':
				level = spell_list.level

		dc = 23 + level

		if school in ['transmutation', 'necromancy', 'conjuration']:
			dc += 1
		if school in ['transmutation']:
			dc += 1

		if highest_level > level:
			# Hardcoding Metamagic
			level += 4
			name = "Quickened " + name
			casting_time = "1 Swift Action"
		else:
			highest_level = level


		content += "\n"
		content += name
		content += '\t' + str(level)
		content += '\t' + str(spell.range)

		if spell.saving_throw:
			content += '\t' + str(spell.saving_throw.replace('\n', ' '))
		else:
			content += '\t'
		if spell.saving_throw == 'none':
			content += '\t'
		else: 
			content += '\t' + str(dc)

		content += '\t' + str(spell.spell_resistance)
		content += '\t' + casting_time
		content += '\t' + school

		area = ""

		if (spell.area and spell.target):
			area = str(spell.area) + " or " + str(spell.target)
		elif (spell.area):
			area = str(spell.area)
		elif (spell.target):
			area = str(spell.target)

		content += '\t' + area.replace('\n', ' ')

	return content


def spellbook(request, character_id):
	content_type = get_content_type(request)
	character_id = normalize_name(character_id)
	data = read_data("characters", character_id)
	data['id'] = character_id
	character = Character(character_id, data)

	if content_type == "text/plain":
		content = get_spell_csv(character.spells)
		return HttpResponse(content.encode('utf-8'), content_type="text/plain")
	else:
		context = get_standard_context(character.spells)
		return render(request, "rules/spells/_spell_list.html.j2", context)

		

def skills(request, skill_name):
	skill_name = normalize_name(skill_name)
	skill = read_dataclass(Skill, skill_name, 'data/skills')
	context = get_standard_context(skill)

	return render(request, "rules/skills/_skill.html.j2", context)

def spells(request, spell_name):
	spell_name = normalize_name(spell_name)
	skill = read_dataclass(Spell, spell_name, 'data/spells')
	context = get_standard_context(skill)

	return render(request, "rules/spells/_spell.html.j2", context)

def all_sources(request):
	sources = Source.objects.all()
	page_model = {
		"title": "Sources",
		"collection": [{
			"Name": f"[{source.name}](sources/{source.fragment})",
			"Copyright Year": str(source.copyright_year),
		} for source in sources
		],
	}
	context = get_standard_context(page_model)
	return render(request, "rules/_collection.html.j2", context)

def source(request, source_frag):
	source_frag = normalize_name(source_frag)
	source = Source.objects.get(fragment=source_frag)
	context = get_standard_context(source)

	return render(request, "rules/_source.html.j2", context)

types = {"feat": Feat, "spell": Spell, "skill": Skill, "weapon": Weapon, "weapon_ability": WeaponAbility}
templates = {
	"feat": "rules/feats/_feat.md.j2",
	"spell": "rules/spells/_spell.md.j2",
	"skill": "rules/skills/_skill.md.j2",
	"weapon": "rules/weapons/_weapon.md.j2",
	"weapon_ability": "rules/weapons/_ability.md.j2",
}

def get_ext(request):
	path_parts = request.path.split('.')

	if len(path_parts) < 2:
		return None
	return path_parts[-1]

def get_content_type(request):
	ext = get_ext(request)
	charset = "; charset=utf-8"

	if ext == "md":
		return "text/markdown" + charset
	elif ext == "txt":
		return "text/plain" + charset
	elif ext == "csv":
		return "text/plain" + charset
	elif ext == "text":
		return "text/plain" + charset
	return "text/markdown" + charset

text_fields = ["benefit", "text", "description", "brief_reference"]

def make_paths_absolute(txt: str):
	# BASE_URL ends with a slash, so we remove the slash from the relative path.
	return txt.replace("](/", f"]({settings.BASE_URL}")

def try_get_dataclass(id: str, clazz, external_target: bool):
	search_result = asdict(read_dataclass(clazz, id))

	if external_target:
		# If we are rendering for something external, relative paths should be made absolute.
		for field in text_fields:
			if field in search_result:
				search_result[field] = make_paths_absolute(search_result[field])

	return search_result

def query(request, query_frag, type_frag=None):
	content_type = get_content_type(request)
	query_frag = normalize_name(query_frag)

	if type_frag is not None and type_frag in types:
		search_result = try_get_dataclass(query_frag, types[type_frag], True)
		return render(request,
			templates[type_frag],
			search_result,
			content_type=content_type)

	search_result = None
	for type_ in types.keys():
		try:
			search_result = try_get_dataclass(query_frag, types[type_], True)
		except FileNotFoundError:
			# If the file isn't found, just try the next one in the list.
			pass
		if search_result:
			return render(request,
				templates[type_],
				search_result,
				content_type=content_type)

	raise Http404(f"No results for {query_frag}")

def index(request):
	context = get_standard_context(None)
	context["title"] = "RPG Tools"
	context["text"] = "#RPG Tools\nThis is a collection of tools to help you run a tabletop RPG!"

	return render(request, "_page.html.j2", context)

def licenses(request, license):
	context = get_standard_context(None)
	if (license.startswith("agpl")):
		context["title"] = "GNU Affero General Public License"
		path = "LICENSE.md"
	elif (license.startswith("ogl")):
		context["title"] = "Open Game License Version 1.0a"
		path = "data/LICENSE.md"
	else:
		raise Http404("License not found")

	with open(path, encoding='utf-8') as file:
		context["text"] = file.read()

	return render(request, "_page.html.j2", context)

def debug(request):



	debug_info = {
		"secure": request.is_secure(),
		"headers": request.headers,
	}
	if request.is_secure():
		logger.info("Recieved secure request, to host: %s", request.headers['Host'])
	else:
		logger.info("Recieved insecure request, to host: %s", request.headers['Host'])
	

	# As a simple way to verify auth is working, 
	# we allow system users to see the response, rather than just logging it.
	if request.user.groups.filter(name='system').exists():
		return HttpResponse(str(debug_info).encode('utf-8'))
	else:
		logger.info(debug_info)

	return HttpResponse(b"OK")
