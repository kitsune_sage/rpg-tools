from collections import namedtuple
import logging
import os
import re
import urllib.request

from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)
URL_REGEX = re.compile(r"(https?)://([a-z.0-9]*)/([^#]*)(.*)")

Url = namedtuple('Url', ['scheme', 'host', 'path', 'fragment'])

# D20 is giving me a 410 when not setting the user-agent.
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'

def parse_url(url):
	matches = URL_REGEX.match(url)
	scheme = matches.group(1)
	host = matches.group(2)
	path = matches.group(3)
	fragment = matches.group(4)
	return Url(scheme, host, path, fragment)

def CachedSoup(url, cache_dir=".cache") -> BeautifulSoup:
	# Used a cache during dev. may as well keep, incase I want to run this a couple times
	parsed_url = parse_url(url)
	path_parts = list(os.path.split(parsed_url.path.replace(':', '_')))

	if parsed_url.path.endswith('/'):
		path_parts.append('index.html')
	cache_path = os.path.join(cache_dir, parsed_url.host, *path_parts)



	try:
		with open(cache_path, 'r', encoding="utf-8") as file:
			return BeautifulSoup(file, features="html.parser")
	except FileNotFoundError:
		try:
			os.makedirs(os.path.dirname(cache_path))
		except FileExistsError:
			logger.warn(f"Not found in cache. Making request to {url}")

		req = urllib.request.Request(
				url = url,
				headers={
					'User-Agent': USER_AGENT
				}
			)
		with urllib.request.urlopen(req) as response:
			encoding = response.info().get_content_charset('utf-8')
			with open(cache_path, 'w', encoding="utf-8") as file:
				raw_content = response.read()
				decoded = raw_content.decode(encoding)
				file.write(decoded)

	with open(cache_path, 'r', encoding="utf-8") as file:
		return BeautifulSoup(file, features="html.parser")
