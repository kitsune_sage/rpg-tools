"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from dataclasses import dataclass
from typing import ClassVar, List
import os

@dataclass
class Source():
    """Data class for sources."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "sources")

    id: str
    name: str
    section_15: List[str]
    copyright_year: int
    publisher: str
    authors: List[str]


    def __str__(self):
        copyright_str = (" © " + str(self.copyright_year)) if self.copyright_year else ""
        publisher = (", " + str(self.publisher)) if self.publisher else ""

        if self.authors:
            authors = ", ".join(self.authors)
            if len(self.authors) > 1:
                start, last = authors.rsplit(",", maxsplit=1)
                authors = start + ", and" + last
            authors = "; Authors: " + authors
        else:
            authors = ""

        return self.name + copyright_str + publisher + authors
