"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from typing import ClassVar, Dict, List
import os

from dataclasses import dataclass, field

from ..helpers import validate_mandatory_fields

@dataclass
class Descriptor():
    """The various descriptors that may be applied to a spell."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "descriptors")
    name: str
    sources: List[str]
    text: str

@dataclass
class SpellListEntry():
    name: str
    level: int


@dataclass
class _BaseSpell():
    """Data class for spell."""
    DIRECTORY: ClassVar[str] = os.path.join("data", "spells")
    id: str
    name: str = None
    sources: List[str] = None
    school: str = None
    subschool: str = None
    descriptors: List[str] = field(default_factory=list)
    spell_lists: List[SpellListEntry] = None
    casting_time: str = None
    components: List[str] = None
    range: str = None
    effect: str = None
    duration: str = None
    saving_throw: str = None
    spell_resistance: str = None
    area: str = None
    target: str = None
    text: str = None

    @property
    def school_text(self):
        text = self.school
        if self.subschool:
            text += ' (' + self.subschool + ")"
        return text

    @property
    def descriptors_text(self):
        if not self.descriptors:
            return None
        return "[" + ", ".join(self.descriptors) + "]"

    @property
    def link(self):
        return f"spells/{self.id}.html"

    @property
    def d20_link(self):
        first_char = self.name[0].lower()
        return f"https://www.d20pfsrd.com/magic/all-spells/{first_char}/{self.id}/"

    @property
    def aon_link(self):
        return f"https://www.aonprd.com/SpellDisplay.aspx?ItemName={self.name}"


# TODO: Currently not a good way to do Self Referencing Dataclasses, so this is a hacky workaround.
# Should be able to use `from typing_extension import Self` once python 3.11 is genrally available.
@dataclass
class Spell(_BaseSpell):
    functions_as: _BaseSpell = None

    def __post_init__(self):
        missing = list()

        if self.functions_as:
            if not self.components:
                self.components = self.functions_as.components
            if not self.casting_time:
                self.casting_time = self.functions_as.casting_time
            if not self.saving_throw:
                self.saving_throw = self.functions_as.saving_throw
            if not self.school:
                self.school = self.functions_as.school
                if not self.subschool:
                    self.subschool = self.functions_as.subschool
            if not self.range:
                self.range = self.functions_as.range

            functions_as_mandatory = [
                "components",
                "school",
                "casting_time",
            ]

            for field_name in functions_as_mandatory:
                if getattr(self, field_name) is None and \
                    getattr(self.functions_as_spell, field_name) is None:
                    missing.append(field_name)

        mandatory = [
            "sources",
            "spell_lists",
            "text",
        ]
        validate_mandatory_fields(self, mandatory)

@dataclass
class SpellsPerDay():
    DIRECTORY: ClassVar[str] = os.path.join("data", "spells-per-day-progression")
    spell_levels_by_class_level: List[Dict[int, int]]
    id: str

    def spells_for_level(self, level) -> Dict[int, int]:
        current_spell_levels = dict()
        current_level = 0
        for spell_levels in self.spell_levels_by_class_level:
            current_level += 1
            if current_level <= level:
                current_spell_levels = current_spell_levels | spell_levels
        return current_spell_levels
