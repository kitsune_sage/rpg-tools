"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from copy import copy
from ..equipment import Armor
from ...data_readers import read_dataclass
import dataclasses

def read_custom_armor(armor_dict: dict):
    if isinstance(armor_dict, str):
        armor_dict = {'data_source': armor_dict}
    else:
        armor_dict = copy(armor_dict)
    enhancement = 0
    masterwork = False
    try:
        enhancement = armor_dict.pop('enhancement')
    except KeyError:
        # If enhancement doesn't exist, no problem.
        pass

    try:
        if not enhancement:
            masterwork = armor_dict.pop('masterwork')
    except KeyError:
        pass

    armor = read_dataclass(Armor, armor_dict)
    # If we have an instance cached, we don't want these changes included multiple times.
    armor = dataclasses.replace(armor)

    armor.armor_bonus += enhancement
    if enhancement or masterwork:
        armor.armor_check_penalty += 1

    effective_enhancement = 0
    ability_name = ""
    ability_price = 0

    for ability in armor.armor_ability:
        effective_enhancement += ability.bonus_cost
        ability_name += ability.name
        ability_price += ability.price

    if enhancement:
        armor.name = f"+{enhancement} {ability_name} {armor.name}"
        if 'cost' not in armor_dict:
            # Make sure it wasn't overridden before we try changing it.
            # 300 for masterwork quality.
            effective_enhancement += enhancement
            armor.cost = armor.cost + (effective_enhancement * effective_enhancement * 1000) + 150 + ability_price
    elif armor.armor_ability:
        armor.name = f"{ability_name} {armor.name}"
        if 'cost' not in armor_dict:
            # Make sure it wasn't overridden before we try changing it.
            # 300 for masterwork quality.
            armor.cost = armor.cost + (effective_enhancement * effective_enhancement * 1000) + 150 + ability_price
    elif masterwork:
        armor.name = f"Masterwork {armor.name}"
        if 'cost' not in armor_dict:
            # Make sure it wasn't overridden before we try changing it.
            # 300 for masterwork quality.
            armor.cost = armor.cost + 300

    return armor
