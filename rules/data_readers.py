"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import dataclasses
from decimal import Decimal, InvalidOperation, getcontext
import os
from typing import get_args, get_origin

from .data_models import Spell

import yaml


class DictToDictConverter():
    def can_convert(self, src, dst_type):
        return get_origin(dst_type) is dict and isinstance(src, dict)

    def convert(self, src, dst_type, path):
        # If we have a dict[Skill, Feature], we need to convert the keys and values seperately.
        # This flow isn't currently hit, but should still be valid.
        inner_classes = get_args(dst_type)
        key_class = inner_classes[0]
        value_class = inner_classes[1]

        new_dict = {}
        converted_mapping = {}
        for key, value in src.items():
            converted_key = convert_to_dataclass(key_class, key, path)
            converted_value = convert_to_dataclass(value_class, value, path)
            converted_mapping[converted_key] = converted_value
        return converted_mapping


class ListConverter():
    def can_convert(self, src, dst_type):
        return get_origin(dst_type) is list and isinstance(src, list)

    def convert(self, src, dst_type, path):
        inner_class = get_args(dst_type)[0]
        return list(map(lambda x: convert_to_dataclass(inner_class, x, path), src))


class CastConverter():
    supported_casts = {
        int: (str, Decimal)
    }
    def can_convert(self, src, dst_type):
        if isinstance(src, dst_type):
            return True
        return type(src) in self.supported_casts and dst_type in self.supported_casts[type(src)]

    def convert(self, src, dst_type, path):
        return dst_type(src)


class NumberConverter():
    def can_convert(self, src, dst_type):
        return dst_type == int or dst_type == Decimal

    def convert(self, src, dst_type, path):
        if isinstance(src, float):
            src = ('%.2f' % src).rstrip('0').rstrip('.')

        try:
            return dst_type(src)
        except ValueError:
            pass
        except InvalidOperation:
            pass

        if '*' in src:
            # For now, the only equation we need to support is multiplication. Shouldn't be
            # to difficult to add more if the need arises.
            parts = src.split('*')
            product = 1
            for multiplier in parts:
                product *= self.convert(multiplier, dst_type, path)
            return product
        raise ValueError(f"{src} isn't a number")


class DictToDataClassConverter():
    def can_convert(self, src, dst_type):
        return isinstance(src, dict)

    def convert(self, src, dst_type, path):
        if 'data_source' in src:
            dict_data = merge(src, read_data(dst_type.DIRECTORY, src.pop('data_source')))
        else:
            dict_data = src

        return dict_to_class(dst_type, dict_data, path)


class FileReferenceConverter():
    def can_convert(self, src, dst_type):
        return isinstance(src, str) or isinstance(src, int)

    def convert(self, src, dst_type, path):
        return read_dataclass(dst_type, str(src), directory=None)


        return Calculation.create(self.data.get('hp', dict()), defaults, self.calc_values, 'base')


converters = [
    DictToDictConverter(),
    ListConverter(),
    CastConverter(),
    NumberConverter(),
    DictToDataClassConverter(),
    FileReferenceConverter(),
]


def convert_to_dataclass(dataclass, value, path):
    """Convert the provided value into the requested dataclass, using a variety of methods based on
    the provided values.

    Args:
        dataclass (type): the type to convert to.
        value: The value to be converted.

    Raises:
        Exception: If no conversion method is found.

    Returns:
        (dataclass): The converted value.
    """

    for converter in converters:
        if converter.can_convert(value, dataclass):
            return converter.convert(value, dataclass, path)

    raise Exception(f'No converter could handle {value} to type {dataclass.__name__}')


def read_data(directory: str, data_name: str) -> dict:
    """Read the data, based on the name, and the location of a file.

    Args:
        directory (str): The directory the data is in.
        data_name (str): The name of the data

    Returns:
        dict: The loaded contents of the file.
    """
    replacements = {}

    if '.' in data_name:
        split_data = data_name.split('.')
        base_name = split_data[0]
        for file in os.listdir(directory):
            if file.startswith(base_name + '.'):
                filename = os.path.join(directory, file)
                split_name = file.split('.')

                for i in range(1, len(split_data)):
                    if split_name[i] == 'spell':
                        spell = convert_to_dataclass(Spell, split_data[i], filename + '#spell')
                        replacements['spell.name'] = spell.name

                    replacements[split_name[i]] = split_data[i]

    else:
        filename = os.path.join(directory, f'{data_name}.yml')

    with open(filename, encoding='utf-8') as file:
        data = yaml.load(file, yaml.FullLoader)

    if '.' in data_name:
        replace_values(data, replacements)
    data['id'] = data_name
    return data


def replace_values(data, replacements):
    for data_key in data:
        if isinstance(data[data_key], str):
            for rep_key in replacements:
                data[data_key] = data[data_key].replace(f'{{{rep_key}}}', replacements[rep_key])

        if isinstance(data[data_key], dict):
            replace_values(data[data_key], replacements)


class ParseError(Exception):
    def __init__(self, path):
        message = f"Failed to parse {path}"
        super().__init__(message)

def dict_to_class(dataclass, mapping: dict, path: str):
    """Converts the provided dictionary, into the dataclass. It finds all keys that match a field
    on the dataclass, and converts each one into the type expected by the dataclass.

    Args:
        dataclass: The class to convert to.
        mapping (dict): The dictionary to convert

    Returns:
        (dataclass): A new instance of the dataclass.
    """
    for field in dataclasses.fields(dataclass):
        if field.name in mapping:
            current_path = path + f".{field.name}"
            try:
                mapping[field.name] = convert_to_dataclass(
                    field.type,
                    mapping[field.name],
                    current_path)
            except Exception as ex:
                raise ParseError(current_path) from ex

    try:
        return dataclass(**mapping)
    except Exception as ex:
        raise ParseError(path) from ex


def read_dataclass(dataclass, value, directory=None):
    directory = directory or dataclass.DIRECTORY

    if isinstance(value, dict):
        value = value.copy()
        path = f"{directory}#{value}"
        if 'data_source' in value:
            dict_data = merge(value, read_data(directory, value.pop('data_source')))
        else:
            dict_data = value
    else:
        path = os.path.join(directory, f"{value}.yml")
        path = f'"{path}"'
        dict_data = read_data(directory, value)
        dict_data['id'] = value
    class_data = dict_to_class(dataclass, dict_data, path=path)

    return class_data


def merge(primary_dict: dict, secondary_dict: dict) -> dict:
    """Return a dict that has any keys from primary_dict, as well as any keys from secondary_dict.
    If a key is a dictionary in both of the inputs, merge those dictionaries.

    Args:
        primary_dict (dict): The primary input
        secondary_dict (dict): The secondary input

    Returns:
        dict: A merged dictionary
    """
    merged = primary_dict.copy()
    for key, value in secondary_dict.items():
        if key not in merged:
            merged[key] = value
        elif isinstance(merged[key], dict) and isinstance(value, dict):
            merged[key] = merge(merged[key], value)

    return merged


def read_dir(dataclass, path=None):
    """Read each file from the directory, and create an instance of the dataclass from it.

    This skips over files that have a name that start with '.'

    Args:
        dataclass ([type]): The dataclass to convert to.
        path (str, optional): The path of the directory to read. Defaults to None.

    Returns:
        list: A list of dataclasses, one for each file in the array.
    """
    if path is None:
        path = f'{dataclass.DIRECTORY}'
    results = list()
    for fname in sorted(filter(lambda name: name.endswith('.yml'), os.listdir(path))):
        if fname.startswith('.'):
            # Allows me to comment out files, without losing them.
            continue
        try:
            data = read_dataclass(dataclass, fname[0:-4], path)

            results.append(data)
        except TypeError:
            print(f'unable to convert {fname}')
            raise
    return results
